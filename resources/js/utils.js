/*
Author: Colom Boyle
Class: CSC-490 AI
Instructor: Dr. Kim
Final Project: Tower Defense Game

Contains a variety of utility methods for an HTML canvas environment. Provides pathing code (A*), PriorityQueue data
structure, math utility methods, array utility methods, adjustable color object, etc.

*/

const TWO_PI = 2 * Math.PI;
const HALF_PI = Math.PI / 2;
const QUARTER_PI = Math.PI / 4;

const MAX_DEGREES = 360;
const HALF_DEGREES = MAX_DEGREES / 2;
const QUARTER_DEGREES = MAX_DEGREES / 4;

class Point {
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }

    distance(point) {
        return Math.hypot(this.x - point.x, this.y - point.y);
    }

    toString() {
        return this.x.toString() + ', ' + this.y.toString();
    }
}

class PQNode {
    constructor(item, priority, index) {
        this.item = item;
        this.priority = priority;
        this.index = index;
    }

    toString() {
        return this.item.toString();
    }
}

class PriorityQueue {
    constructor(priorityCallback) {
        this.heap = [null];
        this.priorityCallback = priorityCallback;
    }

    insert(item) {
        let index = this.size() + 1;
        let node = new PQNode(item, this.priorityCallback(item), index);
        let parent = this.parent(node);
        this.heap.push(node);

        while (parent !== null && node.priority < parent.priority) {

            // swap node and parent
            this.swapNodes(node, parent);

            // update parent
            parent = this.parent(node);
        }
    }

    remove() {
        let root = this.peek();
        let node = this.heap[this.heap.length - 1];
        this.heap.pop();

        if (this.size() > 0) {
            this.heap[root.index] = node;
            node.index = root.index;

            let leftChild = this.leftChild(node);
            let rightChild = this.rightChild(node);

            while ((this.hasLeftChild(node) && node.priority > leftChild.priority) || (this.hasRightChild(node) && node.priority > rightChild.priority)) {
                let children = this.children(node);
                let swapNode;

                if (children.length === 1) {
                    swapNode = children[0];
                } else if (leftChild.priority < rightChild.priority) {
                    swapNode = leftChild;
                } else {
                    swapNode = rightChild;
                }
                this.swapNodes(node, swapNode);

                leftChild = this.leftChild(node);
                rightChild = this.rightChild(node);
            }
        }

        return root.item;
    }

    peek() {
        if (!this.isEmpty()) {
            return this.root();
        }
    }

    contains(item, callbackEquals) {
        for (let i = 1; i < this.heap.length; i++) {
            if ((typeof callbackEquals === 'function' && callbackEquals(this.heap[i].item, item))) {
                return true;
            } else if (item === this.heap[i]) {
                return true;
            }
        }

        return false;
    }

    isEmpty() {
        return this.size() === 0;
    }


    size() {
        return this.heap.length - 1;
    }

    root() {
        return this.heap[1];
    }

    parent(node) {
        return this.heap[Math.floor(node.index / 2)];
    }

    children(node) {
        let children = [];

        if (this.hasLeftChild(node)) {
            children.push(this.leftChild(node));
        }

        if (this.hasRightChild(node)) {
            children.push(this.rightChild(node));
        }

        return children;
    }

    leftChild(node) {
        let index = node.index * 2;
        if (index < this.heap.length) {
            return this.heap[index];
        }

        return null;
    }

    rightChild(node) {
        let index = node.index * 2 + 1;
        if (index < this.heap.length) {
            return this.heap[index];
        }

        return null;
    }

    hasLeftChild(node) {
        return this.leftChild(node) !== null;
    }

    hasRightChild(node) {
        return this.rightChild(node) !== null;
    }

    hasChildren(node) {
        return this.hasLeftChild(node) || this.hasRightChild(node);
    }

    swapNodes(node1, node2) {
        let temp = node1;
        let tempIndex = node1.index;

        this.heap[node1.index] = node2;
        this.heap[node2.index] = temp;

        node1.index = node2.index;
        node2.index = tempIndex;
    }

    toString() {
        return this.heap.toString();
    }
}

class AStarNode {
    constructor(item, gCost, hCost) {
        this.parent = null;
        this.item = item;
        this.gCost = gCost;
        this.hCost = hCost;
    }

    fCost() {
        return this.gCost + this.hCost;
    }

    toString() {
        return 'parent: ' + this.parent + ', item: ' + this.item.toString() + ', gCost: ' + this.gCost + ', hCost: ' + this.hCost + ', fCost: ' + this.fCost();
    }

}

class Path {
    constructor() {
        this.cells = [];
    }

    next() {
        if (!this.isEmpty()) {
            let next = this.cells[this.cells.length - 1];
            this.cells.pop();
            return next;
        }

        return null;
    }

    peek() {
        return this.cells[this.size() - 1];
    }

    add(cell) {
        this.cells.push(cell);
        cell.color = 'magenta';
    }

    size() {
        return this.cells.length
    }

    hasNext() {
        return this.size() > 0;
    }

    isEmpty() {
        return this.size() === 0;
    }

    toString() {
        return this.cells.toString();
    }
}


/**
 * A cost estimate used to aid the A* algorithm.
 * @param cell A cell to be examined.
 * @param goalCell The goal cell
 * @param cardinalCost A cost for cardinal direction movement
 * @param diagonalCost A cost for diagonal movement
 * @param modifier A movement modifier
 * @param heuristicType The type of heuristic to be used (diagonal, manhattan, euclidean)
 * @returns {number} Returns a cost estimate
 */
function aStarHeuristic(cell, goalCell, cardinalCost, diagonalCost, modifier, heuristicType = 'diagonal') {

    cardinalCost *= modifier;
    diagonalCost *= modifier;

    let dx = Math.abs(cell.col - goalCell.col);
    let dy = Math.abs(cell.row - goalCell.row);

    if (heuristicType === 'diagonal') {

        return cardinalCost * (dx + dy) + (diagonalCost - 2 * cardinalCost) * Math.min(dx, dy);

    } else if (heuristicType === 'manhattan') {

        return cardinalCost * (dx + dy);

    } else if (heuristicType === 'euclidean') {

        return cardinalCost * Math.sqrt(dx * dx + dy * dy);
    }
}


/**
 * Generates an optimal path from a start cell to a goal cell.
 * @param cells An array of Cell objects
 * @param startCell The starting cell
 * @param goalCell The goal cell
 * @param isTraversableCallback A function to determine if a node is traversable
 * @param terrainMultiplierCallback A function used to get a terrain multiplier based on the calling unit
 * @returns {Path} Returns an array of Cell objects representing the optimal path
 */
function aStarPath(cells, startCell, goalCell, isTraversableCallback, terrainMultiplierCallback = null) {
    let cardinalCost = 10;
    let diagonalCost = 14;

    let path = new Path();

    let openContains = function (node1, node2) {
        return node1.item.isIdenticalCoordinates(node2.item) && node1.gCost === node2.gCost
            && node1.hCost === node2.hCost;
    };

    // create new min priority queue that prioritizes by a cell's fCost
    let open = new PriorityQueue(function (cell) {
        return cell.fCost();
    });
    let closed = [];

    let node = new AStarNode(startCell, 0, 0);
    open.insert(node);

    while (open.size() > 0) {
        let currentNode = open.remove();
        let currentCell = currentNode.item;

        // currentCell.color = 'orange';

        closed.push(currentNode);

        // if goal cell
        if (currentNode.item.isIdenticalCoordinates(goalCell)) {
            addCellsToPath(path, currentNode, goalCell);
            return path;
        }

        // get neighboring cells
        let neighbors = arrayNeighbors(cells, currentCell.row, currentCell.col);

        // iterate over neighboring cells
        for (let i = 0; i < neighbors.length; i++) {
            let neighborCell = neighbors[i];
            let gCost, hCost = 0;
            let multiplier = 1;

            if (terrainMultiplierCallback) {
                multiplier = 1 / terrainMultiplierCallback(neighborCell);
            }

            // if cell is diagonally adjacent, use diagonal cost, else use cardinal cost
            if (isDiagonalNeighbor(currentCell, neighborCell)) {
                gCost = diagonalCost;
            } else {
                gCost = cardinalCost;
            }

            gCost += currentNode.gCost;
            hCost = aStarHeuristic(currentCell, goalCell, cardinalCost, diagonalCost, multiplier);

            let neighborNode = new AStarNode(neighborCell, gCost, hCost);

            // if is traversable or goal cell and not in closed array
            if ((isTraversableCallback(neighborNode.item) || neighborNode.item.isIdenticalCoordinates(goalCell))
                && !arrayContains(closed, neighborNode, function (node1, node2) {
                    return node1.item.isIdenticalCoordinates(node2.item);
                })) {

                let newCost = currentNode.gCost + aStarHeuristic(currentNode.item, neighborNode.item, cardinalCost,
                    diagonalCost, multiplier);

                if (newCost < neighborNode.gCost || !open.contains(neighborNode, openContains)) {
                    neighborNode.gCost = newCost;

                    neighborNode.hCost = aStarHeuristic(neighborNode.item, goalCell, cardinalCost, diagonalCost,
                        multiplier);
                    neighborNode.parent = currentNode;

                    if (!open.contains(neighborNode, openContains)) {
                        // neighborNode.item.color = 'orange';
                        // console.log('gCost:', neighborNode.gCost, 'hCost:', neighborNode.hCost, 'fCost:', neighborNode.fCost());
                        open.insert(neighborNode);
                    }
                }
            }
        }
    }

    return path;
}

function addCellsToPath(path, aStarNode, goalCell) {
    // create path from cells
    while (aStarNode.parent && aStarNode.parent !== null) {
        if (!aStarNode.item.isIdenticalCoordinates(goalCell) || aStarNode.item.isEmpty()) {
            path.add(aStarNode.item);
            // aStarNode.item.color = 'magenta';
        }
        aStarNode = aStarNode.parent;
    }
}

class RgbaColor {
    constructor(red, green, blue, alpha) {
        this.red = red;
        this.green = green;
        this.blue = blue;
        this.alpha = 1;
        if (alpha || alpha === 0) {
            this.alpha = alpha;
        }
    }

    randomize() {
        const max = 255;
        this.red = randomInt(max, true);
        this.green = randomInt(max, true);
        this.blue = randomInt(max, true);
    }

    toString() {
        return 'rgba(' + this.red + ', ' + this.green + ', ' + this.blue + ', ' + this.alpha + ')';
    }
}

RgbaColor.maxValue = 255;

function removeFromArray(array, item, isEqualCallback) {
    for (let i = 0; i < array.length; i++) {
        let currentItem = array[i];
        if (isEqualCallback(currentItem, item)) {
            array.splice(i, 1);
            return currentItem;
        }
    }

    return null;
}

function randomInt(max, isInclusive = false) {
    if (isInclusive) {
        max++;
    }
    return Math.floor(Math.random() * Math.floor(max));
}

function randomFloat(max) {
    return Math.random() * max;
}

function randomIntInRange(min, max, isInclusive = false) {
    min = Math.ceil(min);
    max = Math.floor(max);
    if (isInclusive) {
        max++;
    }
    return Math.floor(Math.random() * (max - min)) + min;
}

function randomFloatInRange(min, max) {
    return Math.random() * (max - min) + min;
}

function toRadians(degrees) {
    return degrees * Math.PI / HALF_DEGREES;
}

function toDegrees(angle) {
    return angle * (Math.PI / HALF_DEGREES);
}

function getAngleRadians(x1, y1, x2, y2) {
    return Math.atan2(y2 - y1, x2 - x1);
}

function getAngleDegrees(x1, y1, x2, y2) {
    return Math.atan2(y2 - y1, x2 - x1) * HALF_DEGREES / Math.PI;
}

/**
 * Creates dx and dy values based on angle and distance input.
 * @param angle Angle in radians.
 * @param distance Distance value.
 * @param isInvertedY Multiplies dy-value by -1 if true. (Should be true if traveling up across y-axis decreases the y-value).
 * @returns {{dx: number, dy: number}} Returns dx and dy values.
 */
function getDxDy(angle, distance, isInvertedY) {
    let dx = distance * Math.cos(angle);
    let dy = distance * Math.sin(angle);
    if (isInvertedY) {
        dy *= -1;
    }
    return {
        dx: dx,
        dy: dy
    };
}

/**
 * Creates dx and dy values based on angle and distance input. Automatically returns dy-value multiplied by -1 to
 * accommodate Canvas dimension system.
 * @param angle Angle in radians.
 * @param distance Distance value.
 * @returns {{dx: number, dy: number}} Returns dx and dy values (dy is multiplied by -1).
 */
function getDxDyCanvas(angle, distance) {
    return getDxDy(angle, distance, true);
}

function isInBounds2d(array, row, col) {
    return row >= 0 && col >= 0 && row < array.length && col < array[0].length;
}

function arrayNeighbors(array, row, col, offset = 0) {
    // let iterations = 3;
    let iterations = 3 + offset * 2;
    let neighbors = [];
    for (let i = 0; i < iterations; i++) {
        for (let j = 0; j < iterations; j++) {
            let rowIndex = row - 1 - offset + i;
            let colIndex = col - 1 - offset + j;

            if (isInBounds2d(array, rowIndex, colIndex) && (rowIndex !== row || colIndex !== col)) {
                neighbors.push(array[rowIndex][colIndex]);
            }
        }
    }

    return neighbors;
}

function isDiagonalNeighbor(cell, neighborCell) {
    let maxDifference = 1;
    return cell.row !== neighborCell.row && cell.col !== neighborCell.col
        && Math.abs(cell.row - neighborCell.row) <= maxDifference
        && Math.abs(cell.col - neighborCell.col) <= maxDifference;
}

function isAdjacent(row1, col1, row2, col2) {
    return Math.abs(row1 - row2) <= 1 && Math.abs(col1 - col2) <= 1;
}

function arrayContains(array, item, equalsCallback) {
    for (let i = 0; i < array.length; i++) {
        if (equalsCallback(array[i], item)) {
            return true;
        }
    }

    return false;
}

function roundDecimals(value, decimals) {
    return Number(Math.round(value + 'e' + decimals) + 'e-' + decimals);
}

function isAABBCollision(x1, y1, width1, height1, x2, y2, width2, height2) {
    return x1 < x2 + width2 && x1 + width1 > x2 && y1 < y2 + height2 && y1 + height1 > y2;
}

function mousePosCanvas(canvas, event) {
    let rect = canvas.getBoundingClientRect();
    return {
        x: event.clientX - rect.left,
        y: event.clientY - rect.top
    };
}