/*
Author: Colom Boyle
Class: CSC-490 AI
Instructor: Dr. Kim
Final Project: Tower Defense Game

This file contains the majority of the game code. It defines game objects, intialization code, and animation/update code.
For A* pathing code and data structures, see 'utils.js'.
 */

const canvas = document.querySelector('canvas');
const context = canvas.getContext('2d');

canvas.style.backgroundColor = 'black';

let cellSize = 0;
let cellRows = 0;
let cellCols = 0;

/**
 * A parent object that other game objects are descended from.
 */
class GameObject extends CanvasObject {
    constructor(context, faction, x, y, size, angle, damage, health, attackInterval, speed, color) {
        super(context, x, y, 0, 0, color, null);
        this.id = game.nextId();
        this.faction = faction;
        this.targetPoint = null;
        this.size = size;
        this.angle = angle;
        this.baseDamage = damage;
        this.damage = damage;
        this.baseHealth = health;
        this.health = health;
        this.attackInterval = attackInterval;
        this.lastAttackMs = -this.attackInterval;
        this.baseSpeed = speed;
        this.speed = speed;
        this.showHitRegion = false;
    }

    move(point = null) {
        if (point) {
            this.x = point.x;
            this.y = point.y;
        } else if (this.targetPoint && this.speed !== 0) {
            let targetDistance = Math.hypot(this.x - this.targetPoint.x, this.y - this.targetPoint.y);
            this.updateAngle();

            let movements;

            if (targetDistance < this.speed) {
                movements = getDxDy(this.angle, targetDistance);
                this.targetPoint = null;
            } else {
                movements = getDxDy(this.angle, this.speed);
            }

            this.x += Math.round(movements.dx);
            this.y += Math.round(movements.dy);
        }
    }

    hitRegion(useXYWidthHeight = true) {
        let topLeft = new Point(Math.round(this.x - this.size), Math.round(this.y - this.size));
        if (useXYWidthHeight) {
            let sizeDoubled = this.size * 2;

            return {
                x: topLeft.x,
                y: topLeft.y,
                width: sizeDoubled,
                height: sizeDoubled
            };
        } else {
            let topRight = new Point(Math.round(this.x + this.size), topLeft.y);
            let bottomRight = new Point(topRight.x, Math.round(this.y + this.size));
            let bottomLeft = new Point(topLeft.x, bottomRight.y);

            return [topLeft, topRight, bottomRight, bottomLeft];
        }

    }

    drawHitRegion() {
        let hitRegion = this.hitRegion();
        drawRect(this.context, hitRegion.x, hitRegion.y, hitRegion.width, hitRegion.height, 'red', false);
    }

    drawHealthBar() {

        let maxBarWidth = cellSize * .75;
        let barHeight = cellSize * .18;
        let baseOffsetX = cellSize - maxBarWidth;
        let baseOffsetY = cellSize * .6;
        let barWidth = maxBarWidth * this.healthPercentage();
        let barX = Math.round(this.x - this.size + baseOffsetX / 2);
        let barY = this.y - baseOffsetY;
        let backgroundColor = 'silver';
        let healthColor;

        if (this.faction === factions.player) {
            healthColor = 'lime';
        } else if (this.faction === factions.enemy) {
            healthColor = 'red';
        } else {
            healthColor = 'yellow';
        }

        // draw background bar
        drawRect(this.context, barX, barY, maxBarWidth, barHeight, backgroundColor, true);

        // draw border
        this.context.strokeStyle = 'black';
        this.context.strokeRect(barX, barY, maxBarWidth, barHeight);

        // draw health bar
        drawRect(this.context, barX, barY, barWidth, barHeight, healthColor, true);
    }

    draw() {
        if (this.showHitRegion) {
            this.drawHitRegion();
        }
        drawArc(this.context, this.x, this.y, cellSize / 2 * .8, 0, TWO_PI, false, this.color.toString(), true);
        this.drawHealthBar();
    }

    updateAngle(angle = null) {
        if (angle !== null) {
            this.angle = angle;
        } else if (this.targetPoint) {
            this.angle = getAngleRadians(this.x, this.y, this.targetPoint.x, this.targetPoint.y);
        }
    }

    hasHealth() {
        return this.health > 0;
    }

    healthPercentage() {
        return this.health / this.baseHealth;
    }

    isAttackReady() {
        return game.now >= this.lastAttackMs + this.attackInterval;
    }

    updateAttackMs() {
        this.lastAttackMs = game.now;
    }

    update() {
        this.move();
        this.draw();
    }
}

GameObject.isEqual = function (gridGameObject1, gridGameObject2) {
    return gridGameObject1.id === gridGameObject2.id;
};

/**
 * A grid-based version of GameObject. Moves across the grid instead of being able to move freely about the map.
 */
class GridGameObject extends GameObject {
    constructor(context, faction, cell, size, angle, damage, health, attackInterval, speed,
                isTraversableCallback = GridGameObject.isTraversable, color) {
        let centerPoint = cell.centerPoint();
        super(context, faction, centerPoint.x, centerPoint.y, size, angle, damage, health, attackInterval,
            speed, color);
        this.isTraversableCallback = isTraversableCallback;
        this.cell = cell;
        this.goalCell = null;
        this.path = null;
        this.terrainMultiplierCallback = GridGameObject.terrainMultiplierCallback;
    }

    draw() {
        let size = Math.round(cellSize * .5);
        let angleOffset = TWO_PI * .02;

        let offset1 = getDxDy(this.angle, size);
        let point1 = new Point(this.x + offset1.dx, this.y + offset1.dy);

        let offset2 = getDxDy(this.angle + TWO_PI / 3 + angleOffset, size);
        let point2 = new Point(this.x + offset2.dx, this.y + offset2.dy);

        let offset3 = getDxDy(this.angle + TWO_PI / 3 * 2 - angleOffset, size);
        let point3 = new Point(this.x + offset3.dx, this.y + offset3.dy);

        drawTriangle(this.context, point1.x, point1.y, point2.x, point2.y, point3.x, point3.y, this.color, true);

        this.drawHealthBar();
    }

    update() {
        if (this.hasHealth()) {
            this.updateCell();
            this.updateSpeed();
            this.handlePathing();
            this.updateAngle();
            this.move();
            this.attack();
            this.draw();
        } else {
            // console.log('unit removed');
            this.cell.removeUnit(this, GameObject.isEqual);
        }
    }

    attack() {
        if (isAdjacent(this.cell.row, this.cell.col, this.goalCell.row, this.goalCell.col) && this.goalCell.hasCores()) {
            let core = this.goalCell.getCore();
            this.updateAngle(getAngleRadians(this.x, this.y, core.x, core.y));

            if (this.isAttackReady()) {
                this.updateAttackMs();

                core.health = Math.max(core.health - this.damage, 0);

                if (!core.hasHealth()) {
                    this.goalCell.removeCore(core, GameObject.isEqual);
                }
                // console.log('core health:', core.health);
            }
        }
    }

    updateCell() {
        let currentCell = game.getCellByCoordinates(this.x, this.y);

        if (!currentCell.isIdenticalCoordinates(this.cell)) {
            currentCell.addUnit(this.cell.removeUnit(this, GameObject.isEqual));
            this.cell = currentCell;
        }
    }

    updateSpeed() {
        this.speed = Math.round(this.baseSpeed * this.terrainMultiplierCallback(this.cell));
    }

    handlePathing() {
        let cell = null;
        let isGoalSurrounded = this.goalCell.isSurrounded();

        if (this.goalCell && !isGoalSurrounded) {
            cell = this.goalCell;
        } else if (this.goalCell && isGoalSurrounded) {
            cell = this.goalCell.randomEmptyNeighbor(1, 5);
        }

        if (cell.isSurrounded()) {
            cell = this.goalCell.randomEmptyNeighbor(1, 5);
        }


        if (cell && !this.path) {
            // console.log('path created');
            // generate new path
            this.generateNewPath(cell);

        } else if ((cell && this.path && !this.targetPoint && this.path.hasNext()
            && !this.isTraversableCallback(this.path.peek()))
            || (this.path && this.path.isEmpty() && !isGoalSurrounded && !(this.cell.isIdenticalCoordinates(this.goalCell))) || this.cell.isAdjacent(this.goalCell)) {

            this.generateNewPath(cell);
            // console.log('path updated');

        } else if (cell && this.path && !this.targetPoint && this.path.hasNext()) {

            this.targetPoint = this.path.next().centerPoint();

        } else if (cell && this.path && !this.targetPoint && !this.path.hasNext()) {

            this.targetPoint = null;

        } else if (!cell) {

            this.path = null;
        }
    }

    generateNewPath(cell = null) {
        if (!cell) {
            this.path = aStarPath(game.cells, this.cell, this.goalCell, this.isTraversableCallback,
                this.terrainMultiplierCallback);
        } else {
            this.path = aStarPath(game.cells, this.cell, cell, this.isTraversableCallback,
                this.terrainMultiplierCallback);
        }
    }
}

GridGameObject.isTraversable = function (cell) {
    return cell.isEmpty() && cell.terrain !== game.terrain.water;
};

GridGameObject.terrainMultiplierCallback = function (cell) {
    if (cell.terrain === game.terrain.standard) {

        return 1;

    } else if (cell.terrain === game.terrain.sand) {

        return .8;

    } else if (cell.terrain === game.terrain.mountain) {

        return .25;

    } else if (cell.terrain === game.terrain.water) {

        return 0;

    }

    return null;
};

class SkirmisherUnit extends GridGameObject {
    constructor(context, faction, cell, angle) {
        let size = Math.floor(cellSize / 2 * .8);
        let damage = 65;
        let attackInterval = 750;
        let health = 100;
        let speed = Math.round(cellSize * .15);
        let color = 'yellow';

        super(context, faction, cell, size, angle, damage, health, attackInterval, speed,
            GridGameObject.isTraversable, color);
    }
}

class Terrain {
    constructor(type, color) {
        this.type = type;
        this.color = color;
    }
}

class Projectile extends GameObject {
    constructor(context, faction, x, y, angle, damage, speed, color) {
        let size = Math.round(cellSize * .1);
        super(context, faction, x, y, size, angle, damage, 50, 0, speed, color);
        let distanceData = getDxDy(angle, speed);
        this.dx = distanceData.dx;
        this.dy = distanceData.dy;
        this.isPierce = true;
    }

    handleCollision() {
        let cell = game.getCellByCoordinates(this.x, this.y);

        if (!this.collision(cell)) {
            let neighbors = arrayNeighbors(game.cells, cell.row, cell.col);

            for (let i = 0; i < neighbors.length; i++) {
                if (this.collision(neighbors[i])) {
                    break;
                }
            }
        }
    }

    collision(cell) {
        let isCollision = false;
        if (cell.hasUnits()) {
            let unit = cell.units[0];

            if (unit.faction === factions.enemy) {
                let unitHitArea = unit.hitRegion();
                let projectileHitArea = this.hitRegion();

                isCollision = isAABBCollision(projectileHitArea.x, projectileHitArea.y, projectileHitArea.width,
                    projectileHitArea.height, unitHitArea.x, unitHitArea.y, unitHitArea.width, unitHitArea.height);

                if (isCollision) {
                    this.applyDamage(unit);

                    if (!this.isPierce) {
                        removeFromArray(game.projectiles, this, GameObject.isEqual);
                    }
                }
            }
        }

        return isCollision;
    }

    applyDamage(unit) {
        unit.health = Math.max(unit.health - this.damage, 0);
    }

    draw() {
        // drawRect(this.context, this.x - this.size, this.y - this.size, this.size * 2, this.size * 2,
        //     this.color.toString(), true);

        drawArc(this.context, this.x, this.y, this.size, 0, TWO_PI, false, this.color.toString(), true);
    }

    update() {
        this.x += this.dx;
        this.y += this.dy;

        if (this.isInBounds(canvas)) {
            this.handleCollision();
            this.draw();
        } else {
            removeFromArray(game.projectiles, this, GameObject.isEqual)
        }
    }
}

class Turret extends GridGameObject {
    constructor(context, faction, cell, angle) {

        let size = Math.floor(cellSize / 2 * .95);
        let damage = 15;
        let attackInterval = 100;
        let health = 500;
        let projectileSpeed = Math.round(cellSize * .75);
        let lockRange = Math.round(cellSize * 12);
        let color = 'midnightblue';

        super(context, faction, cell, size, angle, damage, health, attackInterval, 0, null, color);
        this.projectileSpeed = projectileSpeed;
        this.maxSpread = 0.01;
        this.barrelColor = 'white';
        this.target = null;
        this.lockRange = lockRange;
        this.showRadius = true;
        this.isActivated = false;
    }

    draw() {
        drawArc(this.context, this.x, this.y, this.size, 0, TWO_PI, false, this.color.toString(), true);
        let lineData = getDxDy(this.angle, this.size);
        drawLine(this.context, this.x, this.y, this.x + lineData.dx, this.y + lineData.dy, this.barrelColor.toString());

        if (this.showRadius) {
            drawArc(this.context, this.x, this.y, this.lockRange, 0, TWO_PI, false, 'rgba(255, 255, 255, .5)', false);
        }
    }

    update() {
        if (this.isActivated) {
            // if turret is locked on to target
            if (!this.target) {
                let nearestUnitData = null;

                if (game.cores.length > 0) {
                    nearestUnitData = game.nearestUnitData(this.cell);
                }

                if (nearestUnitData && nearestUnitData.distance <= this.lockRange) {
                    this.target = nearestUnitData.unit;
                }
            }

            // if turret has target
            let distance = 0;
            if (this.target) {
                distance = Math.hypot(this.x - this.target.x, this.y - this.target.y);
            }
            if (this.target && this.target.hasHealth() && distance <= this.lockRange) {

                this.angle = getAngleRadians(this.x, this.y, this.target.x, this.target.y);

                if (this.isAttackReady()) {
                    this.updateAttackMs();

                    let angle = randomFloatInRange(this.angle - this.maxSpread, this.angle + this.maxSpread);
                    let projectile = new Projectile(this.context, factions.player, this.x, this.y, angle, this.damage, this.projectileSpeed, 'white');

                    game.projectiles.push(projectile);
                }
            } else if (this.target) {
                this.target = null;
            }
        }

        // console.log('id:', this.id, ' target:', this.target);

        this.draw();
    }
}

class Wall extends GridGameObject {
    constructor(context, cell) {
        let color = 'lightgrey';
        let size = Math.round(cellSize / 2);
        let health = 50000;
        super(context, factions.neutral, cell, size, 0, 0, health, 0, 0, null, color);
    }

    draw() {
        drawRect(this.context, Math.floor(this.x - cellSize / 2), Math.floor(this.y - cellSize / 2), cellSize,
            cellSize, this.color.toString(), true)
    }

    update() {
        this.draw();
    }
}

class Core extends GridGameObject {
    constructor(context, faction, cell) {
        let health = 15000;
        let size = Math.round(cellSize / 2);
        let color = new RgbaColor(0, 255, 255);
        super(context, faction, cell, size, 0, 0, health, 0, 0, null, color);
    }

    draw() {
        let hitRegion = this.hitRegion();
        drawRect(this.context, hitRegion.x, hitRegion.y, hitRegion.width, hitRegion.height, this.color.toString(), true);
        this.drawHealthBar();
    }

    update() {
        if (this.hasHealth()) {
            this.color.alpha = this.healthPercentage();
            this.draw();
        } else {
            removeFromArray(this.cell.cores, this, GameObject.isEqual);
        }
    }
}

class Cell {
    constructor(context, row, col, x, y, width, height, terrain, color) {
        this.units = [];
        this.walls = [];
        this.turrets = [];
        this.cores = [];
        this.context = context;
        this.row = row;
        this.col = col;
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.terrain = terrain;
        this.color = color;

        this.fillStyle = this.color;
    }

    isIdenticalCoordinates(cell) {
        return this.row === cell.row && this.col === cell.col;
    }

    /**
     * Returns the middle of the cell as a Point object.
     * @returns {Point} A Point object containing the middle x and y coordinates of the Cell object.
     */
    centerPoint() {
        return new Point(Math.floor(this.x + this.width / 2), Math.floor(this.y + this.height / 2));
    }

    hasUnits() {
        return this.units.length > 0;
    }

    hasWalls() {
        return this.walls.length > 0;
    }

    hasTurrets() {
        return this.turrets.length > 0;
    }

    hasCores() {
        return this.cores.length > 0;
    }

    isEmpty() {
        return !this.hasUnits() && !this.hasWalls() && !this.hasTurrets() && !this.hasCores();
    }

    isSurrounded() {
        let neighbors = arrayNeighbors(game.cells, this.row, this.col);
        for (let i = 0; i < neighbors.length; i++) {
            if (neighbors[i].isEmpty()) {
                return false;
            }
        }

        return true;
    }

    isAdjacent(cell) {
        return isAdjacent(this.row, this.col, cell.row, cell.col);
    }

    randomEmptyNeighbor(minOffset = 0, iterations = 1) {

        // expand search ring through iterations
        for (let i = 0; i < iterations; i++) {

            let neighbors = arrayNeighbors(game.cells, this.row, this.col, minOffset);
            let emptyNeighbors = [];

            // check if any neighbor is empty
            for (let i = 0; i < neighbors.length; i++) {
                let neighbor = neighbors[i];
                if (neighbor.isEmpty()) {
                    emptyNeighbors.push(neighbor);
                }
            }

            // if empty neighbors found, return random empty neighbor
            if (emptyNeighbors.length > 0) {
                return emptyNeighbors[randomInt(emptyNeighbors.length)];
            }

            minOffset++;
        }

        return null;
    }

    addUnit(unit) {
        this.units.push(unit);
        game.units.push(unit);
    }

    addWall(wall) {
        this.walls.push(wall);
    }

    addTurret(turret) {
        this.turrets.push(turret);
    }

    addCore(core) {
        game.addCore(core, this);
    }

    removeUnit(unit, isEqualCallback) {
        let returnUnit = removeFromArray(this.units, unit, isEqualCallback);
        game.removeUnit(returnUnit, isEqualCallback);
        return returnUnit;
    }

    removeCore(core, isEqualCallback) {
        let returnCore = removeFromArray(this.cores, core, isEqualCallback);
        game.removeCore(returnCore, isEqualCallback);
        return returnCore;
    }

    getCore() {
        if (this.hasCores) {
            return this.cores[0];
        }
    }

    draw() {
        if (this.color) {
            this.context.fillStyle = this.color;
            this.context.fillRect(this.x, this.y, this.width, this.height);
        } else if (this.terrain !== game.terrain.standard) {
            this.context.fillStyle = this.terrain.color;
            this.context.fillRect(this.x, this.y, this.width, this.height);
        }

    }

    update() {
        // this.draw();

        for (let i = 0; i < this.walls.length; i++) {
            this.walls[i].update();
        }

        for (let i = 0; i < this.cores.length; i++) {
            this.cores[i].update();
        }

        for (let i = 0; i < this.turrets.length; i++) {
            this.turrets[i].update();
        }

        for (let i = 0; i < this.units.length; i++) {
            this.units[i].update();
        }
    }

    toString() {
        return '(' + this.row + ', ' + this.col + ')';
    }
}

const factions = {
    player: 0,
    enemy: 1,
    neutral: 2,
    opposite(faction) {
        if (faction === this.enemy) {
            return this.player;
        } else if (faction === this.player) {
            return this.enemy;
        } else {
            return null;
        }
    }
};

const game = {
    now: performance.now,
    id: -1,
    cells: [],
    units: [],
    cores: [],
    projectiles: [],
    terrain: {
        standard: new Terrain(0, 'teal'),
        sand: new Terrain(1, 'sandybrown'),
        mountain: new Terrain(2, 'darkslategrey'),
        water: new Terrain(3, 'deepskyblue')
    },
    isDrawGrid: true,
    gridColor: 'rgb(30, 30, 30)',
    baseMaxCol: 0,
    generateRandomTerrain: false,
    spawnAreas: [],
    spawnIntervalMs: 1000,
    lastSpawnMs: performance.now(),
    heavySpawnInterval: 15,
    maxActiveUnits: 5,
    isSpawnReady() {
        return this.now >= this.lastSpawnMs + this.spawnIntervalMs;
    },
    updateLastSpawn() {
        this.lastSpawnMs = this.now;
    },

    spawnUnit(row, col) {
        if (this.cores.length > 0 && this.units.length < this.maxActiveUnits) {
            let cell = this.cells[row][col];

            if (cell.isEmpty()) {
                let unit = new SkirmisherUnit(context, factions.enemy, cell, 0);
                unit.goalCell = this.cores[0].cell;
                cell.addUnit(unit);
            }
        }
    },
    getCellByCoordinates(x, y) {
        return this.cells[Math.floor(y / cellSize)][Math.floor(x / cellSize)]
    },
    getRandomCell(minRow = 0, minCol = 0) {
        let row = randomIntInRange(minRow, cellRows);
        let col = randomIntInRange(minCol, cellCols);
        return this.cells[row][col];
    },
    addUnit(unit, cell) {
        this.units.push(unit);
        if (cell) {
            cell.units.push(unit);
        }
    },
    removeUnit(unit, isEqualCallback) {
        return removeFromArray(this.units, unit, isEqualCallback);
    },

    addCore(core, cell) {
        this.cores.push(core);
        if (cell) {
            cell.cores.push(core);
        }
    },
    removeCore(core, isEqualCallback) {
        return removeFromArray(this.cores, core, isEqualCallback);
    },
    createBase() {

        // let perimeterOffsetY = Math.floor(cellRows * .15);
        //
        // for (let i = perimeterOffsetY; i < cellRows - perimeterOffsetY; i++) {
        //     let cell = this.cells[i][0];
        //     cell.addWall(new Wall(context, cell));
        // }
        //
        // let row = perimeterOffsetY;
        // let col = 1;
        // let middleSpace = 3;
        // let offsetX = 4;
        // let maxCols = 20;
        //
        // for (let i = 0; i < 2; i++) {
        //     for (let j = col; j < col + maxCols; j++) {
        //         if (j < col + middleSpace + offsetX || j >= col + middleSpace * 2 + offsetX) {
        //             let cell = this.cells[row][j];
        //             cell.addWall(new Wall(context, cell));
        //         }
        //     }
        //     row = cellRows - perimeterOffsetY - 1;
        // }
        //
        // row = perimeterOffsetY;
        // col += maxCols;
        // let halfMainSpace = 3;
        // let middleY = Math.floor(cellRows / 2);
        //
        // for (let i = perimeterOffsetY; i < cellRows - perimeterOffsetY; i++) {
        //     if (i < middleY - halfMainSpace || i >= middleY + halfMainSpace) {
        //
        //         let cell = this.cells[i][col];
        //         cell.addWall(new Wall(context, cell));
        //     }
        // }
        //
        // for (let i = 0; i < 2; i++) {
        //     let cell = this.cells[middleY - halfMainSpace - 1 + i][col + 1];
        //     cell.addWall(new Wall(context, cell));
        //     halfMainSpace *= -1;
        // }

        for (let i = 0; i < 1200; i++) {
            let randomRow = randomInt(cellRows);
            let randomCol = randomInt(cellCols);
            let cell = this.cells[randomRow][randomCol];
            cell.addWall(new Wall(context, cell));
        }

        let coreX = Math.floor(cellCols * .1);
        let coreY = Math.floor(cellRows / 2);
        // let shieldOffset = 5;
        // let shieldSpace = 3;
        //
        // for (let i = coreY - shieldOffset; i < coreY + shieldOffset; i++) {
        //     let cell = this.cells[i][coreX + shieldOffset];
        //     cell.addWall(new Wall(context, cell));
        //
        //     if (i > coreX - shieldSpace && i < coreX + shieldSpace) {
        //         let cell = this.cells[coreX - shieldSpace][i];
        //         cell.addWall(new Wall(context, cell));
        //     }
        // }

        coreY -= 1;
        let coreCell = this.cells[randomInt(cellRows)][randomInt(cellCols)];
        coreCell.addCore(new Core(context, factions.player, coreCell));

    },
    createTurrets() {
        let cell1 = this.cells[11][22];
        cell1.addTurret(new Turret(context, factions.player, cell1, 0));

        let cell2 = this.cells[cell1.row + 7][cell1.col];
        cell2.addTurret(new Turret(context, factions.player, cell2, 0));

        let cell3 = this.cells[10][11];
        cell3.addTurret(new Turret(context, factions.player, cell3, 0));

        let cell4 = this.cells[cell3.row + 9][cell3.col];
        cell4.addTurret(new Turret(context, factions.player, cell4, 0));
    },
    createTerrain() {
        let randomMountains = 12;
        let randomSand = 8;
        let randomWater = 5;

        let mountainsRow = cellRows - 5 - 1;
        let mountainsCol = 22;
        let mountains = 2;

        // add mountains
        for (let i = mountainsRow; i < cellRows; i++) {
            for (let j = mountainsCol; j < Math.min(mountainsCol + mountains, cellCols); j++) {
                this.cells[i][j].terrain = this.terrain.mountain;
            }
            mountains *= 2;
        }

        let lakeMiddleRow = Math.floor(cellRows * .65);
        let lakeMiddleCol = Math.floor(cellCols * .8);
        let lakeWidth = Math.floor(cellRows * .12);

        let sandStartRow = lakeMiddleRow - lakeWidth - 1;
        let sandEndRow = lakeMiddleRow + lakeWidth + 2;
        let sandStartCol = lakeMiddleCol - lakeWidth - 3;
        let sandEndCol = lakeMiddleCol + lakeWidth + 3;
        let offset = 0;

        // add sand for lake
        for (let i = sandStartRow; i < sandEndRow; i++) {
            for (let j = sandStartCol; j < sandEndCol; j++) {
                this.cells[i][j + offset - 2].terrain = this.terrain.sand;
            }
            if (i % 2 === 1) {
                offset++;
            }
        }

        // add lake
        for (let i = lakeMiddleRow - lakeWidth; i < lakeMiddleRow + lakeWidth; i++) {
            for (let j = lakeMiddleCol - lakeWidth; j < lakeMiddleCol + lakeWidth; j++) {
                this.cells[i][j].terrain = this.terrain.water;
            }
        }

        let terrainLength = 18;
        let terrainWidth = 7;
        let terrainStartRow = 0;
        let terrainMiddleCol = this.baseMaxCol + 8;
        let terrainStartCol = Math.floor(terrainMiddleCol - terrainWidth / 2);
        let widthOffset = 0;

        for (let i = terrainStartRow; i < terrainStartRow + terrainLength; i++) {
            for (let j = terrainStartCol + widthOffset; j < terrainStartCol + terrainWidth - widthOffset; j++) {
                if (isInBounds2d(this.cells, i, j)) {
                    this.cells[i][j].terrain = game.terrain.sand;
                }
            }
            if ((i - terrainStartRow + 1) % terrainWidth === 0) {
                widthOffset++;
            }
        }

        // add random terrain
        // for (let i = 0; i < randomMountains; i++) {
        //     let cell = this.getRandomCell(0, this.baseMaxCol);
        //     if (cell.isEmpty()) {
        //         // add random mountain
        //         cell.terrain = this.terrain.mountain;
        //
        //     }
        // }
        //
        // for (let i = 0; i < randomSand; i++) {
        //     let cell = this.getRandomCell(0, this.baseMaxCol);
        //     if (cell.isEmpty()) {
        //         cell.terrain = this.terrain.sand;
        //     }
        // }
        //
        // for (let i = 0; i < randomWater; i++) {
        //     let cell = this.getRandomCell(0, this.baseMaxCol);
        //     if (cell.isEmpty()) {
        //         cell.terrain = this.terrain.water;
        //     }
        // }

        if (this.generateRandomTerrain) {
            // generate all cells as terrain type
            for (let i = 0; i < cellRows; i++) {
                for (let j = 0; j < cellCols; j++) {
                    this.cells[i][j].terrain = this.terrain.mountain;
                }
            }

            // add random standard terrain
            for (let i = 0; i < 1500; i++) {
                let row = randomInt(cellRows);
                let col = randomInt(cellCols);

                let cell = this.cells[row][col];
                cell.terrain = this.terrain.standard;
            }
        }
    },
    createSpawnAreas() {
        this.spawnAreas = [];
        let spawnAreaSize = 25;

        let topRow = 0;
        // let topCol = randomIntInRange(this.baseMaxCol, cellCols - spawnAreaSize - 1);
        let topCol = randomIntInRange(0, cellCols - spawnAreaSize - 1);
        let topSpawn = [];
        for (let i = topCol; i < topCol + spawnAreaSize; i++) {
            topSpawn.push(this.cells[topRow][i]);
        }

        let bottomRow = cellRows - 1;
        // let bottomCol = randomIntInRange(this.baseMaxCol, cellCols - spawnAreaSize - 1);
        let bottomCol = randomIntInRange(0, cellCols - spawnAreaSize - 1);
        let bottomSpawn = [];
        for (let i = bottomCol; i < bottomCol + spawnAreaSize; i++) {
            bottomSpawn.push(this.cells[bottomRow][i]);
        }

        let rightRow = randomIntInRange(0, cellRows - 1 - spawnAreaSize);
        let rightCol = cellCols - 1;
        let rightSpawn = [];
        for (let i = rightRow; i < rightRow + spawnAreaSize; i++) {
            rightSpawn.push(this.cells[i][rightCol])
        }

        this.spawnAreas.push(topSpawn, bottomSpawn, rightSpawn);

        for (let i = 0; i < this.spawnAreas.length; i++) {
            for (let j = 0; j < this.spawnAreas[i].length; j++) {
                this.spawnAreas[i][j].color = 'red';
            }
        }
    },
    init() {
        this.baseMaxCol = Math.floor(cellCols * .4);
        this.cells = [];
        for (let i = 0; i < cellRows; i++) {
            this.cells.push([])
        }

        // create cells
        for (let i = 0; i < cellRows; i++) {
            for (let j = 0; j < cellCols; j++) {
                this.cells[i][j] = new Cell(context, i, j, j * cellSize, i * cellSize, cellSize, cellSize,
                    this.terrain.standard, null);
            }
        }

        this.createBase();
        // this.createTurrets();
        // this.createTerrain();
        this.createSpawnAreas();
    },
    drawGrid() {
        // if drawGrid enabled, draw grid lines across canvas
        if (this.isDrawGrid) {
            context.strokeStyle = this.gridColor;

            // draw horizontal grid lines
            for (let i = 0; i < this.cells.length + 1; i++) {
                let y = i * cellSize;
                context.beginPath();
                context.moveTo(0, y);
                context.lineTo(canvas.width, y);
                context.stroke();
            }

            // draw vertical grid lines
            for (let i = 0; i < this.cells[0].length + 1; i++) {
                let x = i * cellSize;
                context.beginPath();
                context.moveTo(x, 0);
                context.lineTo(x, canvas.height);
                context.stroke();
            }
            // reset stroke and fill styles, as save/restore is not used
            context.strokeStyle = null;
            context.fillStyle = null;
        }
    },
    nextId() {
        return ++game.id;
    },
    nearestUnitData(cell) {
        let shortestDistance = 0;
        let index = -1;
        let units = this.units;
        for (let i = 0; i < units.length; i++) {
            let unit = units[i];
            let distance = Math.hypot(cell.x - unit.x, cell.y - unit.y);
            if (!shortestDistance || distance < shortestDistance) {
                shortestDistance = distance;
                index = i;
            }
        }

        if (index >= 0) {
            return {
                unit: game.units[index],
                distance: shortestDistance
            };
        }

        return null;
    },

    updateTime() {
        this.now = performance.now();
    },
    update() {
        this.updateTime();

        if (this.isSpawnReady()) {
            this.updateLastSpawn();

            let randomCellRow = randomInt(this.spawnAreas.length);
            let randomCellCol = randomInt(this.spawnAreas[0].length);

            let cell = this.spawnAreas[randomCellRow][randomCellCol];

            this.spawnUnit(cell.row, cell.col);
        }

        // draw cell backgrounds only (prevents units from being drawn over)
        for (let i = 0; i < this.cells.length; i++) {
            for (let j = 0; j < this.cells[i].length; j++) {
                this.cells[i][j].draw();
            }
        }

        this.drawGrid();

        // update and draw contents of cells
        for (let i = 0; i < this.cells.length; i++) {
            for (let j = 0; j < this.cells[i].length; j++) {
                this.cells[i][j].update();
            }
        }

        for (let i = 0; i < this.projectiles.length; i++) {
            this.projectiles[i].update();
        }
    }
};

/**
 * Starts the game and sets intial values
 */
function init() {
    let sizeRatio = 1;
    let height = innerHeight * sizeRatio;

    cellRows = 30;
    cellSize = Math.floor(height / cellRows);
    // cellCols = Math.floor(width / cellSize);
    cellCols = cellRows * 2;

    setCanvasSize(canvas, cellCols * cellSize, cellRows * cellSize);

    game.init();
}

/**
 * Animation looop
 */
function animate() {
    requestAnimationFrame(animate);
    context.clearRect(0, 0, canvas.width, canvas.height);

    game.update();
}

/**
 * Starts game on window load
 */
window.onload = function () {
    init();
    animate();
};
