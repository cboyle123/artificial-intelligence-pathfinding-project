/*
Author: Colom Boyle
Class: CSC-490 AI
Instructor: Dr. Kim
Final Project: Tower Defense Game

A small set of utility code for HTML Canvas. Provides utility functions for drawing shapes, defining moving shapes,
etc. Largely for shapes and visual effects only.

*/

class Shadow {
    constructor(context, shadowOffsetX, shadowOffsetY, shadowColor, shadowBlur) {
        this.context = context;
        this.shadowOffsetX = shadowOffsetX;
        this.shadowOffsetY = shadowOffsetY;
        this.shadowColor = shadowColor;
        this.shadowBlur = shadowBlur;
    }

    activate() {
        this.context.shadowOffsetX = this.shadowOffsetX;
        this.context.shadowOffsetY = this.shadowOffsetY;
        this.context.shadowColor = this.shadowColor;
        this.context.shadowBlur = this.shadowBlur;
    }

    deactivate() {
        this.context.shadowOffsetX = undefined;
        this.context.shadowOffsetY = undefined;
        this.context.shadowColor = undefined;
        this.context.shadowBlur = undefined;
    }
}

class CanvasObject {
    constructor(context, x, y, dx, dy, color, shadow) {
        this.context = context;
        this.x = x;
        this.y = y;
        this.dx = dx;
        this.dy = dy;
        this.color = color;
        this.shadow = shadow;
    }

    move() {
        this.x += this.dx;
        this.y += this.dy;
    }

    draw() {
        // this.context.save();
        if (this.shadow) {
            this.shadow.activate();
        }
        this.context.fillStyle = this.color.toString();
        this.context.fillRect(this.x, this.y, 1, 1);
        // this.context.restore();
    }

    update() {
        this.move();
        this.draw();
    }

    reverseDx() {
        this.dx *= -1;
    }

    reverseDy() {
        this.dy *= -1;
    }

    isInBounds(canvas) {
        return this.x >= 0 && this.x < canvas.width && this.y >= 0 && this.y < canvas.height;
    }

    getAngle() {
        return getAngleRadians(this.x, this.y, this.x + this.dx, this.y + this.dy);
    }
}

class Circle extends CanvasObject {
    constructor(context, x, y, radius, dx, dy, color, shadow) {
        super(context, x, y, dx, dy, color, shadow);
        this.radius = radius;
    }

    draw() {
        // this.context.save();
        if (this.shadow) {
            this.shadow.activate();
        }
        drawArc(this.context, this.x, this.y, this.radius, 0, TWO_PI, false, this.color.toString(), true);
        // this.context.restore();
    }

    getPoint(angle, distance) {
        if (!angle) {
            angle = 0;
        }

        if (!distance && distance !== 0) {
            distance = this.radius;
        }

        let offset = getDxDy(angle, distance);
        return new Point(this.x + offset.dx, this.y + offset.dy);
    }
}

class Rectangle extends CanvasObject {
    constructor(context, x, y, width, height, dx, dy, color, shadow) {
        super(context, x, y, dx, dy, color, shadow);
        this.width = width;
        this.height = height;
    }

    draw() {
        // this.context.save();
        if (this.shadow) {
            this.shadow.activate();
        }
        this.context.fillStyle = this.color.toString();
        this.context.fillRect(this.x, this.y, this.width, this.height);
        // this.context.restore();
    }

    getMiddleX() {
        return this.x + this.width / 2;
    }

    getMiddleY() {
        return this.y + this.height / 2;
    }

    isInBounds(canvas) {
        return this.x + this.width >= 0 && this.x < canvas.width && this.y + this.height >= 0 && this.y < canvas.height;
    }
}

class TrailedCircle extends Circle {
    constructor(context, x, y, radius, dx, dy, color, shadow, trailColor, trailLength, isDynamic = false, trailRatio = 1) {
        super(context, x, y, radius, dx, dy, color, shadow);
        this.isDynamic = isDynamic;
        this.trailRatio = trailRatio;
        this.trailColor = trailColor;
        this.baseTrailLength = trailLength;
        this.trailLength = this.baseTrailLength;
        this.updateTrailLength();
    }

    draw() {
        // this.context.save();
        if (this.shadow) {
            this.shadow.activate();
        }
        let trail = this.getTrail();
        if (trail.lengthValue > 0) {
            drawTriangle(this.context, trail.tip.x, trail.tip.y, trail.side1.x, trail.side1.y, trail.side2.x, trail.side2.y, this.trailColor.toString(), true);
        }
        drawArc(this.context, this.x, this.y, this.radius, 0, TWO_PI, false, this.color.toString(), true);
        // this.context.restore();
    }

    update() {
        this.x += this.dx;
        this.y += this.dy;
        this.updateTrailLength();
        this.draw();
    }

    updateTrailLength() {
        if (this.isDynamic) {
            this.trailLength = Math.hypot(this.y - this.y + this.dy, this.x - this.x + this.dx) * this.trailRatio;
        } else {
            this.trailLength = this.baseTrailLength;
        }
    }

    getTrail() {
        let angle = getAngleRadians(this.x, this.y, this.x + this.dx, this.y + this.dy) + Math.PI;

        let tipX = this.x + this.trailLength * Math.cos(angle);
        let tipY = this.y + this.trailLength * Math.sin(angle);

        let side1X = this.x + this.radius * Math.cos(angle + HALF_PI);
        let side1Y = this.y + this.radius * Math.sin(angle + HALF_PI);

        let side2X = this.x + this.radius * Math.cos(angle + Math.PI + HALF_PI);
        let side2Y = this.y + this.radius * Math.sin(angle + Math.PI + HALF_PI);

        return {
            lengthValue: this.trailLength,
            angle: angle,
            tip: new Point(tipX, tipY),
            side1: new Point(side1X, side1Y),
            side2: new Point(side2X, side2Y)
        }
    }

    isInBounds(canvas) {
        let trail = this.getTrail();
        return (this.x + this.radius >= 0 && this.x - this.radius < canvas.width && this.y + this.radius >= 0 && this.y - this.radius < canvas.height)
            || (trail.tip.x >= 0 && trail.tip.x < canvas.width && trail.tip.y >= 0 && trail.tip.y < canvas.height);
    }
}

class Spark extends Circle {
    constructor(context, x, y, radius, dx, dy, color, maxFrames, shadow) {
        super(context, x, y, radius, dx, dy, color, shadow);
        this.frames = 0;
        this.maxFrames = maxFrames;
        this.decayStartRatio = .3;
        this.alphaDecay = color.alpha / (maxFrames * (1 - this.decayStartRatio));
    }

    update() {
        this.frames++;
        this.x += this.dx;
        this.y += this.dy;
        if (this.frames >= this.maxFrames * this.decayStartRatio) {
            this.color.alpha -= this.alphaDecay;
        }
        this.draw();
    }
}

class LinearGradient {
    constructor(context, x0, y0, x1, y1, color0, color1, offset0, offset1) {
        this.context = context;
        this.x0 = x0;
        this.y0 = y0;
        this.x1 = x1;
        this.y1 = y1;
        this.color0 = color0;
        this.color1 = color1;
        this.offset0 = offset0;
        this.offset1 = offset1;

        this.gradient = this.context.createLinearGradient(this.x0, this.y0, this.x1, this.y1);
        this.gradient.addColorStop(this.offset0, this.color0.toString());
        this.gradient.addColorStop(this.offset1, this.color1.toString());
    }

    update(x0, y0, x1, y1, color0, color1, offset0, offset1) {
        this.x0 = x0;
        this.y0 = y0;
        this.x1 = x1;
        this.y1 = y1;
        this.color0 = color0;
        this.color1 = color1;
        this.offset0 = offset0;
        this.offset1 = offset1;

        this.gradient = this.context.createLinearGradient(this.x0, this.y0, this.x1, this.y1);
        this.gradient.addColorStop(this.offset0, this.color0.toString());
        this.gradient.addColorStop(this.offset1, this.color1.toString());
    }
}

function prepare(context, color) {
    context.strokeStyle = color;
    context.fillStyle = color;
    context.beginPath();
}

function finalize(context, isFilled, isClosed) {
    if (isFilled) {
        context.fill();
    } else if (!isFilled && isClosed) {
        context.closePath();
        context.stroke();
    } else {
        context.stroke();
    }
}

function drawLine(context, x1, y1, x2, y2, color) {
    // context.save();
    context.strokeStyle = color;
    context.beginPath();
    context.moveTo(x1, y1);
    context.lineTo(x2, y2);
    context.stroke();
    // context.restore();
}

function drawRect(context, x, y, width, height, color, isFilled) {
    // context.save();
    prepare(context, color);

    if (isFilled) {
        context.fillRect(x, y, width, height);
    } else {
        context.strokeRect(x, y, width, height);
    }

    // context.restore();
}

function drawTriangle(context, x1, y1, x2, y2, x3, y3, color, isFilled) {
    // context.save();
    prepare(context, color);

    context.moveTo(x1, y1);
    context.lineTo(x2, y2);
    context.lineTo(x3, y3);

    finalize(context, isFilled, true);
    // context.restore();
}

function drawArc(context, x, y, radius, startAngle, endAngle, anticlockwise, color, isFilled, isClosed) {
    // context.save();
    prepare(context, color);

    context.arc(x, y, radius, startAngle, endAngle, anticlockwise);

    finalize(context, isFilled, isClosed);
    // context.restore();
}

function drawEllipse(context, x, y, radiusX, radiusY, rotation, startAngle, endAngle, anticlockwise, color, isFilled, isClosed) {
    // context.save();
    prepare(context, color);

    context.ellipse(x, y, radiusX, radiusY, rotation, startAngle, endAngle, anticlockwise);

    finalize(context, isFilled, isClosed);
    // context.restore();
}

function drawCubicBezierCurve(context, x1, y1, x2, y2, cp1x, cp1y, cp2x, cp2y, color, isFilled, isClosed) {
    // context.save();
    prepare(context, color);

    context.moveTo(x1, y1);
    context.bezierCurveTo(cp1x, cp1y, cp2x, cp2y, x2, y2);

    finalize(context, isFilled, isClosed);
    // context.restore();
}

function drawPolygon(context, points, color, isFilled, isClosed) {
    if (points.length > 0) {
        let point = points[0];

        context.save();
        prepare(context, color);

        context.moveTo(point.x, point.y);
        for (let i = 1; i < points.length; i++) {
            point = points[i];
            context.lineTo(point.x, point.y);
        }

        finalize(context, isFilled, isClosed);
        context.restore();
    }
}

function setCanvasSize(canvas, width, height) {
    canvas.width = width;
    canvas.height = height;
}